# CitizenFX FiveM C# Server/Client Mod with JSON
### Features
- Newtonson JSON integration
- Vector3 Project Direction (Allows for autorunning)
- Server side player balance (no persistence yet, sorry)
- Server side list of weapons/ammo/prices
- Aggressive zombie spawning
- Money gained (balance incremented) for zombie kills



# NodeJS Project for building Mithril.js front end to use in NUI
### Features
- All? Weapons listed in shop to purchase
- Can buy ammo to restock without re-buying gun
- Individual ammo prices
- Multiple quick ammo amount buttons
- Shopping cart for purchasing multiple weapons/ammos at a time
- Unfinished (~not so working~) Vehicle support in shop as well

![alt text](https://bytebucket.org/kade_hafen/outbreakmod/raw/4174661006b54eb42bff9f88aa9ba08e37c5b241/OutbreakMod.png)

# Installation/Running
 - Copy 'outbreak' folder (lower case) into server-data/resources
 - run command start outbreak on FXServer

# Running
 - On FXServer console run start outbreak


# general

- The zombie spawner has not been tested with multiple players

- Item prices are not balanced
 
- To toggle shop window tap sprint (not hold). If you were moving you will continue to move in current direction