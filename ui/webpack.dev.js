const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: false,
    port: 9000
  },
  mode: 'development',

  entry: {
    obui:'./app/index.js',
    style:'./style/main.scss',
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js',
  },plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
    })
  ],
  module: {
    rules: [{
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it use publicPath in webpackOptions.output
              publicPath: path.resolve(__dirname, 'public')
            }
          },
          "css-loader",
          {
            loader:"sass-loader",
            options:{
              includePaths:require("bourbon-neat").includePaths
            }
          }
        ]
    },{
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
      loader: 'url-loader?limit=30000&name=[name].[ext]'
    }]
  }
};