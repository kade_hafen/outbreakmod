
const m = require('mithril');

const send = require('../lib/send'),
state = require('../state'),
shopItems = require('../components/shop-items');

const Weapons = {
    unselected:[],
    view:vnode=>{
        return [
            m(shopItems, {items:state.items.weapons})
        ];
    }
}

module.exports = Weapons;