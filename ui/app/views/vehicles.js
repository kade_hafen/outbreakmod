
const m = require('mithril');

const send = require('../lib/send'),
state = require('../state'),
shopItems = require('../components/shop-items');

const Vehicles = {
    view:vnode=>{
        return m(shopItems, {items:state.items.vehicles});
    }
}

module.exports = Vehicles;