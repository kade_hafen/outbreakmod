
const m = require('mithril');

const shopTabs = require('../components/shop-tabs');
const shopCart = require('../components/shop-cart');
const shopButtons = require('../components/shop-buttons');
const shopItemGroups = require('../components/shop-item-groups');
const state = require('../state');
const info = require('../lib/info');
const send = require('../lib/send');

const Shop = {
    onbeforeupdate:vnode=>{
        state.cart.Calculate();
    },
    view:vnode=>{
        if (!state.shopVisible) return null;

        vnode.attrs.groups = vnode.attrs.weapons ? ["all","gear","heavy","melee","mg","other","pistol","rifle","shotgun","smg","sniper","thrown"]
        : vnode.attrs.vehicles ? ["all"] : [];
        return m('.shop-container', [
            m('.shop-header', [
                m(shopItemGroups, vnode.attrs),
                m('.shop-toggle-container', [
                    m(`a[href=#].shop-toggle${state.hideTooExpensive?'.active':''}`, {
                        onclick:e=>{
                            e.preventDefault();
                            state.hideTooExpensive = !state.hideTooExpensive;
                            if ( state.hideTooExpensive )
                                state.ownedOnly = false;
                        }
                    },"Affordable"),
                    m(`a[href=#].shop-toggle${state.ownedOnly?'.active':''}`, {
                        onclick:e=>{
                            e.preventDefault();
                            state.ownedOnly = !state.ownedOnly;
                            if ( state.ownedOnly )
                                state.hideTooExpensive = false;
                        }
                    },"Owned"),
                ]),
            ]),
            m('.shop', [
                m(shopTabs, vnode.attrs),
                m('.shop-center', vnode.children),
                m(shopCart),
            ]),
            m(shopButtons),
            m('.clear'),
        ]);
    }
}

module.exports = Shop;