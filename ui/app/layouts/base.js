
const m = require('mithril');

const dev = require('../lib/dev');

const info = require('../lib/info');
const balance = require('../components/balance');

const state = require('../state');

const Base = {
    view:vnode=>{
        return [
            m(info.component),
            m(dev, vnode.attrs),
            m('.base', vnode.children ),
            m(balance),
        ];
    }
}

module.exports = Base;