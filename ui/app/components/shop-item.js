
const m = require('mithril');

const state = require('../state');

const info = require('../lib/info');

const ShopItemAmount = {
    view:vnode=>{
        return m('.shop-item-amount.invert-link', {
            onclick:e=>{
                e.preventDefault();
                e.stopPropagation();
                try{
                    state.cart.TryAdd(vnode.attrs.item, vnode.attrs.amount);
                }catch(e){
                    info.Add(e);
                    console.log(e);
                }
            }
        }, vnode.attrs.amount)
    }
}

const ShopItem = {
    view:vnode=>{
        let item = vnode.attrs.item;
        return m('a[href=#].shop-item', {
            onclick:async e=>{
                e.preventDefault();
                try{
                    state.cart.TryAdd(item);
                }catch(e){
                    info.Add(e);
                    console.log(e);
                }
            }
        }, [
            m(`.shop-item-name`, `${item.name}`),
            m('br'),m('br'),m('br'),m('br'),
            m('.shop-item-price-container', [
                m('.shop-item-ammo-price', item.group!=='melee'&&item.ammoprice?"$"+item.ammoprice:""),
                m(`.shop-item-price${state.getEquipment(item)!==null?'.strike':''}`, "$"+item.price),
            ]),
            item.maxammo===0?null:m('.shop-item-amounts', [0.1,0.25,0.75,1].map(i=>{
                if ( !item.maxammo ) return null;
                const amount = Math.floor(Math.floor( item.maxammo*i / 5 )*5);
                if ( amount < 1 ) return null;
                if ( state.balance - (state.cart.total + (amount*item.ammoprice) ) < 0 ) return null;
                return m(ShopItemAmount, {amount,item});
            }))
        ]);
    }
}

module.exports = ShopItem;