
const m = require('mithril');

const state = require('../state');

const Balance = {
    view:vnode=>{
        return m('.balance', "$"+state.balance);
    }
}

module.exports = Balance;