
const m = require('mithril');

const state = require('../state');

const ShopItemGroups = {
    list:[],
    view:vnode=>{
        if (!vnode.attrs.groups) vnode.attrs.groups = [];
        let half = Math.floor(vnode.attrs.groups.length*0.5);
        return m('.shop-item-groups', vnode.attrs.groups.map(group=>{
            let active = (group === 'all' && state.group === null) || state.group === group;
            return m(`a[href=#].shop-item-group${active?'.active':''}`, {
                onclick:e=>{
                    e.preventDefault();
                    if ( group === 'all' )
                        state.group = null;
                    else
                        state.group = group;
                }
            }, group);
        }));
    }
}

module.exports = ShopItemGroups;