
const m = require('mithril');

const ShopTab = {
    view:vnode=>{
        return m(`a.shop-tab[href=#!/${vnode.attrs.link}]${vnode.attrs.active?'.active':''}`, vnode.attrs.name);
    }
}

const ShopTabs = {
    view:vnode=>{
        return m(".shop-tabs", [
            m(ShopTab, {link:'weapons',name:'Weapons', active:vnode.attrs.weapons}),
            m(ShopTab, {link:'vehicles',name:'Vehicles', active:vnode.attrs.vehicles})
        ]);
    }
}

module.exports = ShopTabs;