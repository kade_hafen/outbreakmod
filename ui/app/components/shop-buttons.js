
const m = require('mithril');

const state = require('../state');

const ShopButtons = {
    view:vnode=>{
        return m('.shop-button-container', state.cart.empty?[]:[
            m('a[href=#].shop-button', {
                onclick:async e=>{
                    e.preventDefault();
                    state.cart.Purchase();
                }
            }, m('.shop-button-text', state.cart.purchasing?"...":"Purchase")),
            m('a[href=#].shop-button', {
                onclick:e=>{
                    e.preventDefault();
                    state.cart.Reset();
                }
            }, m('.shop-button-text', "Reset"))
        ]);
    }
}

module.exports = ShopButtons;