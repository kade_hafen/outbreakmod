
const m = require('mithril');

const state = require('../state');

const ShopCartItem = {
    view:vnode=>{
        const i = vnode.attrs.item;
        return m('.shop-cart-item', [
            m(`h5${i.has?'.strike':''}`, i.item.name),
            i.item.amount ?
                m('div', `x${i.item.amount}`)
            :null,
            m(`h6.right`, "$"+i.cost)
        ]);
    }
}

const ShopCart = {
    view:vnode=>{
        return m('.shop-cart-container', state.cart.empty?null:[
            m('.shop-cart-items', 
                state.cart.items.map(item=>{
                    return m(ShopCartItem, {item});
                }),
            ),
            m('.shop-cart-total', "Total: $"+state.cart.total)
        ]);
    }
}

module.exports = ShopCart;