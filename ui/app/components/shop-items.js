
const m = require('mithril');

const state = require('../state');

const shopItem = require('./shop-item');

const ShopItems = {
    view:vnode=>{
        return m('.shop-items', vnode.attrs.items.map(item=>{
            if ( state.group !== null && item.group && state.group !== item.group )
                return null;
            if ( state.hideTooExpensive && state.cart.total + item.price > state.balance && state.getEquipment(item) === null)
                return null;
            if ( state.ownedOnly && state.getEquipment(item) === null )
                return null;
            return m(shopItem, {item});
        }));
    }
}

module.exports = ShopItems;