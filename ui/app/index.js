

'use strict';

const domready = require('domready');
const m = require('mithril');

const routes = require('./routes');

const state = require('./state');

domready(()=>{
    state.init();
    m.route(document.getElementById("app"), '/weapons', routes);
});
