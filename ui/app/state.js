const EventEmitter = require('events').EventEmitter;

const m = require('mithril');

const send = require('./lib/send');
const cart = require('./lib/cart');
const info = require('./lib/info');

class State extends EventEmitter {
    
    constructor(){
        super();
        this.cart = new cart(this);
        this.shopVisible = false;
        this.items = {};
        this.equipment = {}; //current player equipment
        this.balance = 0;

        //filters
        this.group = null; //currently selected group
        this.hideTooExpensive = true;
        this.ownedOnly = false;
    }

    init(){
        console.log("OBUI Init");
        window.addEventListener('message', event=>this.onMessage(event.data));
        window.addEventListener('keydown', this.onKeyDown.bind(this));
        window.addEventListener('keyup', this.onKeyUp.bind(this));
        send("ready");
    }

    onKeyUp(e){
        if ( e.keyCode == 16 )
            e.preventDefault();
    }

    onKeyDown(e){
        if ( e.keyCode == 16 ) {
            e.preventDefault();
            send('hide');
        }
    }

    onMessage(msg){
        if ( typeof msg !== 'object' || typeof msg.obtype !== 'string' ) return;
        switch(msg.obtype){
            case "buyresult":
                this.emit('buyresult', msg.err);
                break;
            case "redraw":
                break;
            case "toggle":
                this.shopVisible = (msg.show === true);
                break;
            case "equipment":
                this.equipment.weapons = msg.weapons;
                break;
            case "inventory":
                this.items = {};
                this.AddToInventory(msg.weapons, "weapons");
                this.AddToInventory(msg.vehicles, "vehicles");
                break;
            case "balance":
                this.balance = msg.amount;
                break;
            case "info":
                info.Add(msg.msg);
                break;
            case "error":
                info.Add("[ERROR]"+msg.msg);
                break;
            default:
                console.log("Unknown message received:", msg);
        }

        m.redraw();
    }

    AddToInventory(data, type){
        for(let i=0;i<data.length;i++){
            data[i].type = type;
            if ( !this.items.hasOwnProperty(type) )
                this.items[type] = [];
            this.items[type].push(data[i]);
        }
    }

    getEquipment(item){
        if ( !this.equipment.hasOwnProperty(item.type) ) return null;
        const found = this.equipment[item.type].filter(o=>o.hash===item.hash);
        if ( found.length > 0 ) return found[0];
        return null;
    }

}

module.exports = new State();