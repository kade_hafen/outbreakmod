'use strict'

const m = require('mithril');

//layouts
const base = require('./layouts/base');
const shop = require('./layouts/shop');

//views
const weapons = require('./views/weapons');
const vehicles = require('./views/vehicles');

const Routes = {
  '/weapons': {
    render: function(vnode){
        return m(base, m(shop, {weapons:true}, m(weapons)));
    }
  },
  '/vehicles': {
    render: function(vnode){
        return m(base, m(shop, {vehicles:true}, m(vehicles)));
    }
  },
  /*
  '/player': { //regions/cluster
    render: function(vnode){
      return m(base, vnode.attrs, m(server, vnode.attrs, m(regions)));
    }
  },
  */
}

module.exports = Routes;