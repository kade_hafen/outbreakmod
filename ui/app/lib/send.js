
const m = require('mithril');

module.exports = async(name, data)=>{
    try{
        const res = await m.request({
            method:"post",
            url:`http://outbreak/${name}`,
            data,
            useBody: true, //strigify the json object
            timeout:3500,
        });
        return res;
    }catch(e){
        console.log(e.message);
        return false;
    }
    return null;
};