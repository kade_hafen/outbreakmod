
const m = require('mithril');

const send = require('./send');
const info = require('./info');

class Cart {

    constructor(state){
        this.state = state;
        this.Reset();
        this.purchasing = false;
        this.total = 0;
        this.items = [];

        state.on('buyresult', e=>{
            this.purchasing = false;
            if (e) info.Add(e);
            else this.Reset();
        });
    }

    Reset(){
        this.empty = true;
        this.weapons = [];
        this.ammo = [];
        this.vehicle = null;
    }

    async Purchase(){
        if ( this.purchasing ) {
            info.Add('Currently in the process of purchasing, please wait.');
            return;
        }
        if ( this.total < 1 ) {
            info.Add('Nothing to purchase, total is $0.');
            return;
        }

        this.purchasing = true;
        const result = await send("buy", this.ToJson());
        if ( result === false ) info.Add("Request failed, sorry!");
        this.purchasing = false;
        m.redraw();
    }

    ToJson(){
        return JSON.stringify({ 
            vehicle:this.vehicle?this.vehicle.hash:0,
            weapons:this.weapons.map(weapon=>{
                return { hash:weapon.hash,amount:weapon.amount }
            })
        });
    }

    Calculate(){
        const items = [];
        if ( this.vehicle ) items.push(this.vehicle);
        this.weapons.map(m=>items.push(m));
        const calculated = [];
        
        this.total = 0;
        this.items = [];
        for(let i=0;i<items.length;i++){
            let item = items[i];
            let has = this.state.getEquipment(item) !== null;
            let cost = 0;
            if ( !has ) cost += (item.price || 0);
            if ( item.amount && item.amount > 0 )
                cost += (item.ammoprice || 1) * item.amount;
            this.total += cost;
            this.items.push({item,has,cost});
        }
    }

    IsFull(){
        return ( this.weapons.length > 4 );
    }

    TryAdd(item, amount){
        if ( this.purchasing ) throw 'Currently in the process of purchasing, please wait.';
        if ( this.total + item.price > this.state.balance && this.state.getEquipment(item) === null ) throw 'Cannot afford that.';

        if ( item.type === 'vehicles' )
            if ( this.weapons.length > 0 )
                throw 'Can only purchase vehicles by themselves.';
            else
                this.vehicle = item;
        else if ( item.type == 'weapons' )
            if ( this.vehicle !== null )
                throw 'Can only purchase vehicles by themselves.';
            else {
                let index = null;
                for(let i=0;i<this.weapons.length;i++){
                    if ( this.weapons[i].hash === item.hash ){
                        index = i;
                        break;
                    }
                }
                //disallow non-ammo types from adding an amount
                if ( item.ammotype === 0 ) amount = 0;
                if ( typeof amount !== 'number' ) amount = 0;
                if ( index == null )
                    if ( this.IsFull() ) throw 'Purchase list full.';
                    else this.weapons.push(Object.assign({amount},item)); //push a clone
                else
                    this.weapons[index].amount = amount;
            }

        this.empty = false;
    }
}

module.exports = Cart;