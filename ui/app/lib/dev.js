
const m = require('mithril');

const state = require('../state');

function LoadData(){

    state.onMessage(inventory);

    state.onMessage({
        obtype:"equipment",
        weapons:[
            {
                hash: 2937143193
            }
        ]
    });
}

const Dev = {
    oninit:vnode=>{
        state.shopVisible = true;
        LoadData();
    },
    view:vnode=>{
        return m(".abs[style=top:20px;left:20px;]", [
            m("button", {
                onclick:e=>{
                    state.shopVisible = !state.shopVisible;
                    m.redraw();
                }
            }, "Toggle Shop"),
            m('br'),m('br'),
            m("button", {
                onclick:e=>{
                    state.onMessage({
                        obtype:"balance",
                        amount:state.balance+100,
                    });
                    m.redraw();
                }
            }, "Add $100"),
        ])
    }
}

module.exports = Dev;

const inventory = {"obtype":"inventory","vehicles":[{"name":"Adder","price":10000,"hash":3078201489,"category":3}],"weapons":[{"name":"Advanced Rifle","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":2937143193},{"name":"AP Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":584646201},{"name":"Assault Rifle","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":3220176749},{"name":"Assault Rifle Mk2","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":961495388},{"name":"Assault Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":3800352039},{"name":"Assault SMG","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":4024951519},{"name":"Ball","price":10,"ammoprice":10,"group":"thrown","maxammo":1,"hash":600439132},{"name":"Bat","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2508868239},{"name":"BattleAxe","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":3441901897},{"name":"Bottle","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":4192643659},{"name":"Bullpup Rifle","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":2132975508},{"name":"Bullpup Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":2640438543},{"name":"BZ Gas","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":2694266206},{"name":"Carbine Rifle","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":2210333304},{"name":"Carbine Rifle Mk2","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":4208062921},{"name":"Combat MG","price":10,"ammoprice":1,"group":"mg","maxammo":250,"hash":2144741730},{"name":"Combat MG Mk2","price":10,"ammoprice":1,"group":"mg","maxammo":250,"hash":3686625920},{"name":"Combat PDW","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":171789620},{"name":"Combat Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":1593441988},{"name":"Compact Grenade Launcher","price":10,"ammoprice":1,"group":"heavy","maxammo":20,"hash":125959754},{"name":"Compact Rifle","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":1649403952},{"name":"Crowbar","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2227010557},{"name":"Dagger","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2460120199},{"name":"Double Barrel Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":4019527611},{"name":"Fire Extinguisher","price":10,"ammoprice":1,"group":"other","maxammo":2000,"hash":101631238},{"name":"Firework","price":10,"ammoprice":1,"group":"other","maxammo":20,"hash":2138347493},{"name":"Flare","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":1233104067},{"name":"Flare Gun","price":10,"ammoprice":1,"group":"pistol","maxammo":20,"hash":1198879012},{"name":"Flashlight","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2343591895},{"name":"Golf Club","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":1141786504},{"name":"Grenade","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":2481070269},{"name":"Grenade Launcher","price":10,"ammoprice":1,"group":"heavy","maxammo":20,"hash":2726580491},{"name":"Grenade Launcher Smoke","price":10,"ammoprice":1,"group":"other","maxammo":20,"hash":1305664598},{"name":"Gusenberg","price":10,"ammoprice":1,"group":"mg","maxammo":250,"hash":1627465347},{"name":"Hammer","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":1317494643},{"name":"Hatchet","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":4191993645},{"name":"Heavy Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":3523564046},{"name":"Heavy Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":984333226},{"name":"Heavy Sniper","price":10,"ammoprice":1,"group":"sniper","maxammo":250,"hash":205991906},{"name":"Heavy Sniper Mk2","price":10,"ammoprice":1,"group":"sniper","maxammo":250,"hash":177293209},{"name":"Homing Launcher","price":10,"ammoprice":1,"group":"heavy","maxammo":10,"hash":1672152130},{"name":"Knife","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2578778090},{"name":"Knuckle Duster","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":3638508604},{"name":"Machete","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":3713923289},{"name":"Machine Pistol","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":3675956304},{"name":"Marksman Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":3696079510},{"name":"Marksman Rifle","price":10,"ammoprice":1,"group":"sniper","maxammo":250,"hash":3342088282},{"name":"MG","price":10,"ammoprice":1,"group":"mg","maxammo":250,"hash":2634544996},{"name":"Micro SMG","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":324215364},{"name":"Minigun","price":10,"ammoprice":1,"group":"heavy","maxammo":250,"hash":1119849093},{"name":"Mini SMG","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":3173288789},{"name":"Molotov","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":615608432},{"name":"Musket","price":10,"ammoprice":1,"group":"other","maxammo":250,"hash":2828843422},{"name":"Nightstick","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":1737195953},{"name":"Night Vision","price":10,"ammoprice":1,"group":"gear","maxammo":0,"hash":2803906140},{"name":"Parachute","price":10,"ammoprice":1,"group":"gear","maxammo":0,"hash":4222310262},{"name":"Petrol Can","price":10,"ammoprice":1,"group":"other","maxammo":4500,"hash":883325847},{"name":"Pipe Bomb","price":10,"ammoprice":10,"group":"thrown","maxammo":10,"hash":3125143736},{"name":"Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":453432689},{"name":"Pistol 50","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":2578377531},{"name":"Pistol Mk2","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":3219281620},{"name":"Pool Cue","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":2484171525},{"name":"Proximity Mine","price":10,"ammoprice":10,"group":"thrown","maxammo":5,"hash":2874559379},{"name":"Pump Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":487013001},{"name":"Railgun","price":10,"ammoprice":1,"group":"heavy","maxammo":20,"hash":1834241177},{"name":"Revolver","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":3249783761},{"name":"RPG","price":10,"ammoprice":1,"group":"heavy","maxammo":20,"hash":2982836145},{"name":"Sawn-Off Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":2017895192},{"name":"SMG","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":736523883},{"name":"SMG Mk2","price":10,"ammoprice":1,"group":"smg","maxammo":250,"hash":2024373456},{"name":"Smoke Grenade","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":4256991824},{"name":"Sniper Rifle","price":10,"ammoprice":1,"group":"sniper","maxammo":250,"hash":100416529},{"name":"Snowball","price":10,"ammoprice":10,"group":"thrown","maxammo":10,"hash":126349499},{"name":"SNS Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":3218215474},{"name":"Special Carbine","price":10,"ammoprice":1,"group":"rifle","maxammo":250,"hash":3231910285},{"name":"Sticky Bomb","price":10,"ammoprice":10,"group":"thrown","maxammo":25,"hash":741814745},{"name":"Stun Gun","price":10,"ammoprice":1,"group":"other","maxammo":250,"hash":911657153},{"name":"Sweeper Shotgun","price":10,"ammoprice":1,"group":"shotgun","maxammo":250,"hash":317205821},{"name":"Switch Blade","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":3756226112},{"name":"Vintage Pistol","price":10,"ammoprice":1,"group":"pistol","maxammo":250,"hash":137902532},{"name":"Wrench","price":10,"ammoprice":1,"group":"melee","maxammo":0,"hash":419712736}]};