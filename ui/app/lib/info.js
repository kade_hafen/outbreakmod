
const m = require('mithril');

class Info {

    constructor(){
        this.maxMessages = 5;
        this.messages = [];
        this.component = {
            view:vnode=>{
                return m('.info', this.messages.map(msg=>{
                    return m('div', msg);
                }));
            }
        }
        this.timeouts = {};
    }

    Add(msg){
        if ( this.timeouts[msg] )
            clearTimeout(this.timeouts[msg]);
        else
            this.messages.push(msg);

        this.timeouts[msg] = setTimeout(()=>{
            const index = this.messages.indexOf(msg);
            if ( index !== -1 )
                this.messages.splice(index, 1);
            this.timeouts[msg] = undefined;
            m.redraw();
        }, 4000)
        m.redraw();
    }
}

const info = new Info();

module.exports = info;