const fs = require('fs'),
path = require('path');

//cause webpack is weird af
fs.unlinkSync(path.join(__dirname, "public", "style.js"));

fs.readFile(path.join(__dirname, "public", "index.html"), "utf8", (err,html)=>{
    if(err)return console.log(err);
    fs.readFile(path.join(__dirname, "public", "obui.js"), "utf8", (err,obui)=>{
        if(err)return console.log(err);
        fs.readFile(path.join(__dirname, "public", "style.css"), "utf8", (err,css)=>{
            if(err)return console.log(err);
            fs.readFile(path.join(__dirname, "public", "ChaletLondonNineteenSixty.ttf"), (err,font)=>{
                if(err)return console.log(err);
                fs.writeFile(path.join(__dirname, "..", "outbreak", "ui", "ChaletLondonNineteenSixty.ttf"), font, err=>{
                    if(err)return console.log(err);

                    html = html.replace(/<script.+?>/, `<script type='text/javascript'>${obui}</script>`);
                    html = html.replace(/<link.+?>/, `<style type='text/css'>${css}</style>`);
                    html = html.replace(/\s\s+/g, '');

                    //console.log(html);
                    fs.writeFile(path.join(__dirname, "..", "outbreak", "ui", "index.html"), html, err=>{
                        if(err)return console.log(err);
                        console.log("Compiled UI Copied to mod.");
                    }); 
                });
            });
        });
    });
});