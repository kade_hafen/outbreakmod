const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    obui:'./app/index.js',
    style:'./style/main.scss',
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js',
  },plugins: [
    new webpack.NormalModuleReplacementPlugin(
      /lib[\\/]dev\.js/,
      './dummy.js'
    ),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
    })
  ],
  module: {
    rules: [{
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it use publicPath in webpackOptions.output
              publicPath: path.resolve(__dirname, 'public')
            }
          },
          "css-loader",
          {
            loader:"sass-loader",
            options:{
              includePaths:require("bourbon-neat").includePaths
            }
          }
        ]
    }]
  }
};