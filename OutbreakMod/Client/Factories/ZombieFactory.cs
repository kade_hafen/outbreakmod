﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using Common;
using System.Threading.Tasks;

namespace Client
{
    public static class ZombieFactory
    {

        public static async Task<Ped> Create(Vector3 pos)
        {
            Model m = new Model(PedHash.Zombie01);
            m.Request();
            while (!m.IsLoaded)
                await BaseScript.Delay(1);
            Ped zombie = new Ped(API.CreatePed((int)Types.PedType.PED_TYPE_MISSION, (uint)m.Hash, pos.X, pos.Y, pos.Z, API.GetRandomFloatInRange(0, 1), true, false));
            m.MarkAsNoLongerNeeded();

            zombie.RelationshipGroup.SetRelationshipBetweenGroups(Game.PlayerPed.RelationshipGroup, Relationship.Hate, true);
            API.SetAmbientVoiceName(zombie.Handle, "ALIENS");
            zombie.IsPainAudioEnabled = false;
            
            API.SetPedCombatRange(zombie.Handle, 2);
            API.SetPedAlertness(zombie.Handle, 3);
            API.SetPedHearingRange(zombie.Handle, float.MaxValue);
            API.SetPedCombatAttributes(zombie.Handle, 46, true);
            API.SetPedCombatAttributes(zombie.Handle, 5, true);
            API.SetPedCombatAttributes(zombie.Handle, 1, false);
            API.SetPedCombatAttributes(zombie.Handle, 0, false);
            API.SetPedCombatAbility(zombie.Handle, 0);
            API.SetAiMeleeWeaponDamageModifier(float.MaxValue);
            API.SetPedRagdollBlockingFlags(zombie.Handle, 4);
            API.SetPedCanPlayAmbientAnims(zombie.Handle, false);

            //zombie.CanSufferCriticalHits = false;
            if (!API.HasAnimSetLoaded("move_m@alien"))
                API.RequestAnimSet("move_m@alien");

            API.SetPedIsDrunk(zombie.Handle, true);
            if (!API.HasAnimSetLoaded("MOVE_M@DRUNK@VERYDRUNK"))
                API.RequestAnimSet("MOVE_M@DRUNK@VERYDRUNK");
            API.SetPedMovementClipset(zombie.Handle, "MOVE_M@DRUNK@VERYDRUNK", 1f);

            zombie.MaxHealth = 100;
            zombie.Health = 100;
            zombie.Armor = 5;

            zombie.Task.WanderAround();

            return zombie;
        }
    }
}
