﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace Client
{
    public class Vehicle
    {
        public string name = "Unknown";
        
        public int price = 999999;
        
        public VehicleHash hash = VehicleHash.Bmx;
        
        public VehicleClass category = VehicleClass.Cycles;
    }

    public static class Vehicles
    {
        private static List<Vehicle> _All = null;
        

        public static List<Vehicle> All
        {
            get
            {
                if (_All==null)
                    Load();
                return _All;
            }
        }

        private static void Load()
        {
            _All = new List<Vehicle>();

            _All.Add(new Vehicle
            {
                name = "Adder",
                category = VehicleClass.Coupes,
                price = 10000,
                hash = VehicleHash.Adder
            });
        }
    }
}
