﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Client
{
    public class Inventory
    {
        public string obtype;
        
        public List<Vehicle> vehicles = null;
        
        public List<Weapon> weapons = null;

        Inventory()
        {
            weapons = Weapons.All;
            vehicles = Vehicles.All;
        }

        private static Inventory _Instance = null;

        public static Inventory Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Inventory();
                return _Instance;
            }
        }

        public static string ToJson()
        {
            Instance.obtype = "inventory";
            return JsonConvert.SerializeObject(Instance);
        }
    }
}
