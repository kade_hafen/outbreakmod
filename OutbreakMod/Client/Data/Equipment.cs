﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Client
{
    internal class EquipmentWeapon
    {
        public uint hash = 0;
        public int amount = 0;
    }
    
    internal class Equipment
    {
        public string obtype;
        
        public List<EquipmentWeapon> weapons = null;

        Equipment()
        {
            obtype = "equipment";
        }

        private void Update()
        {
            weapons = new List<EquipmentWeapon>();
            Ped player = Game.PlayerPed;
            foreach(WeaponHash hash in Enum.GetValues(typeof(WeaponHash)))
            {
                if (player.Weapons.HasWeapon(hash))
                    weapons.Add(new EquipmentWeapon
                    {
                        hash = (uint)hash,
                        amount = API.GetAmmoInPedWeapon(player.Handle, (uint)hash)
                    });
            }
            
        }

        private static Equipment _Instance = null;

        public static Equipment Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new Equipment();
                return _Instance;
            }
        }

        public static string ToJson()
        {
            Instance.Update();
            return JsonConvert.SerializeObject(Instance);
        }
    }
}
