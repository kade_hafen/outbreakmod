﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace Client
{
    public enum WeaponDamageType
    {
        Unknown = 0,
        NoDamage = 1,
        Melee = 2,
        Bullet = 3,
        ForceRagdollFall = 4,
        Explosive = 5,
        Fire = 6,
        Fall = 8,
        Electric = 10,
        BarbedWire = 11,
        Extinguisher = 12,
        Gas = 13,
        WaterCannon = 14,
    }
    
    public class Weapon
    {
        public string name = "Unknown";
        
        public int price = 99999;

        public int ammoprice = 999;
        
        public string group = "other";

        public int maxammo = -1;
        
        public WeaponHash hash = WeaponHash.Unarmed;
    }

    public static class Weapons
    {
        private static List<Weapon> _All = null;
        

        public static List<Weapon> All
        {
            get
            {
                if (_All == null)
                    Load();
                return _All;
            }
        }

        internal static int GetPriceTotal(List<CartWeapon> cartWeapons)
        {
            int total = 0;
            foreach(CartWeapon cartWeapon in cartWeapons)
            {
                Weapon data;
                try
                {
                    data = All.Find(w => (uint)w.hash == cartWeapon.hash);
                }
                catch (Exception){
                    data = new Weapon(); //go with defaults
                }
                if (!Game.Player.Character.Weapons.HasWeapon(((WeaponHash)cartWeapon.hash)))
                    total += data.price;
                if (!data.group.Equals("melee"))
                    total += (cartWeapon.amount * data.ammoprice);
            }
            return total;
        }

        private static void Add(WeaponHash hash, string name, int price)
        {
            Add(hash, name, price, 1);
        }

        private static void Add(WeaponHash hash, string name, int price, int ammoprice)
        {
            uint hsh = (uint)hash;

            int maxammo = 0;
            API.GetMaxAmmo(Game.PlayerPed.Handle, hsh, ref maxammo);

            string group = GetGroup(hash);
            _All.Add(new Weapon
            {
                name = name,
                price = price,
                ammoprice = group.Equals("thrown") ? price : ammoprice,
                group = group,
                maxammo = maxammo,
                hash = hash,
            });
        }

        private static string GetGroup(WeaponHash hash)
        {
            int groupint = API.GetWeapontypeGroup((uint)hash);
            WeaponGroup group = WeaponGroup.Unarmed;
            Enum.TryParse(groupint.ToString(), out group);

            if ( group == 0 )
            {
                switch (hash)
                {
                    case WeaponHash.MachinePistol:
                    case WeaponHash.AssaultSMG:
                    case WeaponHash.CombatPDW:
                    case WeaponHash.MicroSMG:
                    case WeaponHash.MiniSMG:
                    case WeaponHash.SMG:
                    case WeaponHash.SMGMk2:
                        group = WeaponGroup.SMG;
                        break;
                    case WeaponHash.Bat:
                    case WeaponHash.BattleAxe:
                    case WeaponHash.Bottle:
                    case WeaponHash.Crowbar:
                    case WeaponHash.Flashlight:
                    case WeaponHash.Dagger:
                    case WeaponHash.GolfClub:
                    case WeaponHash.Hammer:
                    case WeaponHash.Hatchet:
                    case WeaponHash.Knife:
                    case WeaponHash.KnuckleDuster:
                    case WeaponHash.Machete:
                    case WeaponHash.Nightstick:
                    case WeaponHash.PoolCue:
                    case WeaponHash.SwitchBlade:
                    case WeaponHash.Wrench:
                        group = WeaponGroup.Melee;
                        break;
                    case WeaponHash.MarksmanRifle:
                    case WeaponHash.SniperRifle:
                    case WeaponHash.HeavySniper:
                    case WeaponHash.HeavySniperMk2:
                        group = WeaponGroup.Sniper;
                        break;
                    case WeaponHash.GrenadeLauncher:
                    case WeaponHash.CompactGrenadeLauncher:
                    case WeaponHash.HomingLauncher:
                    case WeaponHash.Minigun:
                    case WeaponHash.Railgun:
                    case WeaponHash.RPG:
                        group = WeaponGroup.Heavy;
                        break;
                    case WeaponHash.NightVision:
                        group = WeaponGroup.NightVision;
                        break;
                }
            }

            switch (group)
            {
                case WeaponGroup.Heavy:
                    return "heavy";
                case WeaponGroup.Melee:
                    return "melee";
                case WeaponGroup.MG:
                    return "mg";
                case WeaponGroup.Pistol:
                    return "pistol";
                case WeaponGroup.Shotgun:
                    return "shotgun";
                case WeaponGroup.SMG:
                    return "smg";
                case WeaponGroup.AssaultRifle:
                    return "rifle";
                case WeaponGroup.Sniper:
                    return "sniper";
                case WeaponGroup.Thrown:
                    return "thrown";
                case WeaponGroup.DigiScanner:
                case WeaponGroup.NightVision:
                case WeaponGroup.Parachute:
                case WeaponGroup.FireExtinguisher:
                    return "gear";
            }
            return "other";
        }

        private static void Load()
        {
            _All = new List<Weapon>();

            Add(WeaponHash.AdvancedRifle, "Advanced Rifle", 400);
            Add(WeaponHash.APPistol, "AP Pistol", 100);
            Add(WeaponHash.AssaultRifle, "Assault Rifle", 200);
            Add(WeaponHash.AssaultRifleMk2, "Assault Rifle Mk2", 600);
            Add(WeaponHash.AssaultShotgun, "Assault Shotgun", 800, 3);
            Add(WeaponHash.AssaultSMG, "Assault SMG", 200);
            Add(WeaponHash.Ball, "Ball", 1);
            Add(WeaponHash.Bat, "Bat", 20);
            //Add(WeaponHash.BattleAxe, "BattleAxe", 1); //given for free on spawn
            Add(WeaponHash.Bottle, "Bottle", 5);
            Add(WeaponHash.BullpupRifle, "Bullpup Rifle", 100);
            Add(WeaponHash.BullpupShotgun, "Bullpup Shotgun", 100);
            Add(WeaponHash.BZGas, "BZ Gas", 80);
            Add(WeaponHash.CarbineRifle, "Carbine Rifle", 400);
            Add(WeaponHash.CarbineRifleMk2, "Carbine Rifle Mk2", 600);
            Add(WeaponHash.CombatMG, "Combat MG", 10);
            Add(WeaponHash.CombatMGMk2, "Combat MG Mk2", 10);
            Add(WeaponHash.CombatPDW, "Combat PDW", 300);
            Add(WeaponHash.CombatPistol, "Combat Pistol", 100);
            Add(WeaponHash.CompactGrenadeLauncher, "Compact Grenade Launcher", 100, 4);
            Add(WeaponHash.CompactRifle, "Compact Rifle", 10);
            Add(WeaponHash.Crowbar, "Crowbar", 20);
            Add(WeaponHash.Dagger, "Dagger", 40);
            Add(WeaponHash.DoubleBarrelShotgun, "Double Barrel Shotgun", 100, 2);
            Add(WeaponHash.FireExtinguisher, "Fire Extinguisher", 10);
            Add(WeaponHash.Firework, "Firework", 1800, 5);
            Add(WeaponHash.Flare, "Flare", 10);
            Add(WeaponHash.FlareGun, "Flare Gun", 100);
            Add(WeaponHash.Flashlight, "Flashlight", 10);
            Add(WeaponHash.GolfClub, "Golf Club", 20);
            Add(WeaponHash.Grenade, "Grenade", 10);
            Add(WeaponHash.GrenadeLauncher, "Grenade Launcher", 1000, 10);
            Add(WeaponHash.GrenadeLauncherSmoke, "Grenade Launcher Smoke", 10);
            Add(WeaponHash.Gusenberg, "Gusenberg", 400);
            Add(WeaponHash.Hammer, "Hammer", 10);
            Add(WeaponHash.Hatchet, "Hatchet", 10);
            Add(WeaponHash.HeavyPistol, "Heavy Pistol", 10);
            Add(WeaponHash.HeavyShotgun, "Heavy Shotgun", 10);
            Add(WeaponHash.HeavySniper, "Heavy Sniper", 10);
            Add(WeaponHash.HeavySniperMk2, "Heavy Sniper Mk2", 10);
            Add(WeaponHash.HomingLauncher, "Homing Launcher", 10);
            Add(WeaponHash.Knife, "Knife", 10);
            Add(WeaponHash.KnuckleDuster, "Knuckle Duster", 10);
            Add(WeaponHash.Machete, "Machete", 10);
            Add(WeaponHash.MachinePistol, "Machine Pistol", 10);
            Add(WeaponHash.MarksmanPistol, "Marksman Pistol", 10);
            Add(WeaponHash.MarksmanRifle, "Marksman Rifle", 10);
            Add(WeaponHash.MG, "MG", 10);
            Add(WeaponHash.MicroSMG, "Micro SMG", 10);
            Add(WeaponHash.Minigun, "Minigun", 10);
            Add(WeaponHash.MiniSMG, "Mini SMG", 10);
            Add(WeaponHash.Molotov, "Molotov", 10);
            Add(WeaponHash.Musket, "Musket", 10);
            Add(WeaponHash.Nightstick, "Nightstick", 10);
            Add(WeaponHash.NightVision, "Night Vision", 10);
            Add(WeaponHash.Parachute, "Parachute", 10);
            Add(WeaponHash.PetrolCan, "Petrol Can", 10);
            Add(WeaponHash.PipeBomb, "Pipe Bomb", 10);
            Add(WeaponHash.Pistol, "Pistol", 10);
            Add(WeaponHash.Pistol50, "Pistol 50", 10);
            Add(WeaponHash.PistolMk2, "Pistol Mk2", 10);
            Add(WeaponHash.PoolCue, "Pool Cue", 10);
            Add(WeaponHash.ProximityMine, "Proximity Mine", 10);
            Add(WeaponHash.PumpShotgun, "Pump Shotgun", 10);
            Add(WeaponHash.Railgun, "Railgun", 10);
            Add(WeaponHash.Revolver, "Revolver", 10);
            Add(WeaponHash.RPG, "RPG", 10);
            Add(WeaponHash.SawnOffShotgun, "Sawn-Off Shotgun", 10);
            Add(WeaponHash.SMG, "SMG", 10);
            Add(WeaponHash.SMGMk2, "SMG Mk2", 10);
            Add(WeaponHash.SmokeGrenade, "Smoke Grenade", 10);
            Add(WeaponHash.SniperRifle, "Sniper Rifle", 10);
            Add(WeaponHash.Snowball, "Snowball", 10);
            Add(WeaponHash.SNSPistol, "SNS Pistol", 10);
            Add(WeaponHash.SpecialCarbine, "Special Carbine", 10);
            Add(WeaponHash.StickyBomb, "Sticky Bomb", 10);
            Add(WeaponHash.StunGun, "Stun Gun", 10);
            Add(WeaponHash.SweeperShotgun, "Sweeper Shotgun", 10);
            Add(WeaponHash.SwitchBlade, "Switch Blade", 10);
            Add(WeaponHash.VintagePistol, "Vintage Pistol", 10);
            Add(WeaponHash.Wrench, "Wrench", 10);
        }
    }
}
