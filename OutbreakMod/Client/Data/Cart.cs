﻿
using System;
using System.Collections.Generic;
using CitizenFX.Core.Native;
using Newtonsoft.Json;

namespace Client
{
    internal class CartWeapon
    {
        public uint hash = 0;
        //hold the ammo amount
        public int amount = 0;
    }

    internal class Cart
    {
        public uint vehicle = 0;//json vars

        public List<CartWeapon> weapons = new List<CartWeapon>();//json vars

        internal static Cart FromJson(string json)
        {
            return JsonConvert.DeserializeObject<Cart>(json);
        }

        internal int CalculateCost()
        {
            int total = 0;
            if (!vehicle.Equals(0))
            {
                total = 5000; //TODO Cost of vehicles
            }
            else
            {
                total = Weapons.GetPriceTotal(weapons);
            }
            
            return total;
        }

        internal string GiveItems()
        {
            int cost = CalculateCost();

            if (cost > PlayerController.Balance)
            {
                return "Not enough money, go kill more zombies.";
            }

            if (!vehicle.Equals(0))
            {
                return "Car Spawning not implemented yet, sorry.";
            }
            else
            {
                try
                {
                    foreach (CartWeapon weapon in weapons)
                    {
                        API.GiveWeaponToPed(API.GetPlayerPed(-1), weapon.hash, Math.Max(1, weapon.amount), false, weapons[0].Equals(weapon));
                    }
                }catch(Exception)
                {
                    return "Something went wrong, sorry";
                }
            }
            API.SendNuiMessage(Equipment.ToJson());
            PlayerController.AddToBalance(-cost);
            return null;
        }
    }
}
