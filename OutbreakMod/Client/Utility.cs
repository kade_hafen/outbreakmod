﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;

namespace Client
{
    internal static class Utility
    {
        internal static PlayerList _playerList = new PlayerList();

        internal static double DegreesPerRadian = 180 / Math.PI;

        internal static Player GetClosestPlayer(Vector3 pos, out float distance)
        {
            distance = float.MaxValue;
            float temp = 0;
            Player player = null;
            foreach (Player p in _playerList)
            {
                temp = Vector3Distance2(p.Character.Position, pos);
                if (temp < distance)
                {
                    distance = temp;
                    player = p;
                }
            }
            return player;
        }

        internal static float Vector3Distance2(Vector3 from, Vector3 to)
        {
            return API.Vdist2(from.X, from.Y, from.Z, to.X, to.Y, to.Z);
        }

        internal static void Vector3Project(ref Vector3 pos, float degrees, float distance)
        {
            degrees = degrees + 90;
            if (degrees > 360)
                degrees -= 360;
            double radians = degrees / DegreesPerRadian;
            pos.X = (float)(pos.X + distance * Math.Cos(radians));
            pos.Y = (float)(pos.Y + distance * Math.Sin(radians));
            pos.Z = World.GetGroundHeight(pos);
        }
    }
}
