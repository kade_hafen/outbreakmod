﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    internal class PlayerController : BaseScript
    {
        internal static int _Balance = 0;

        internal static int Balance
        {
            get
            {
                return _Balance;
            }
        }

        public PlayerController()
        {
            EventHandlers["playerSpawned"] += new Action(OnPlayerSpawned);
            EventHandlers["outbreak:balance"] += new Action<int>(balance => {
                _Balance = balance;
                UpdateNuiBalance();
            });

            Tick += OnFrame;
        }

        internal static void AddToBalance(int amount)
        {
            TriggerServerEvent("outbreak:balance:add", new object[] { amount });
        }

        internal static void UpdateNuiBalance()
        {
            API.SendNuiMessage(String.Format("{{\"obtype\":\"balance\",\"amount\":{0}}}", _Balance));
        }

        private async Task OnFrame()
        {
            LocalPlayer.WantedLevel = 0;
            Game.Player.Character.Armor = 100000;
            await Task.FromResult(0);
        }

        void OnPlayerSpawned()
        {
            API.GiveWeaponToPed(Game.Player.Character.Handle, (uint)WeaponHash.Hatchet, 1, false, true);

            if (_Balance < 100)
            {
                API.GiveWeaponToPed(Game.Player.Character.Handle, (uint)WeaponHash.VintagePistol, 40, false, true);
                UIController.SendInfo("Cause you broke af, here is a free gun.");
            }
        }
    }
}
