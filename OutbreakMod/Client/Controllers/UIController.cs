﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Concurrent;

namespace Client
{
    internal class GeneralResult
    {
        public string obtype = null;
        public string err;
    }

    internal class Position
    {
        public string obtype = null;
        public Vector3 position;
    }

    internal class UIController : BaseScript
    {
        public int button = 21; //sprint

        private bool _hasControl = false;
        private int i = 0;
        private bool _buttonDown = false;

        private bool _autoRun = false;
        private bool _autoRunning = false;
        private Vector3 _pos;
        private Vector3 _autoRunDestination;

        public UIController()
        {
            API.RegisterNuiCallbackType("ready");
            EventHandlers["__cfx_nui:ready"] += new Action<string>(OnReady);
            API.RegisterNuiCallbackType("buy");
            EventHandlers["__cfx_nui:buy"] += new Action<string>(OnBuy);
            API.RegisterNuiCallbackType("hide");
            EventHandlers["__cfx_nui:hide"] += new Action<string>(body=>ReleaseControl());

            Tick += OnFrame;
            Tick += UpdateAutorun;
        }

        private async Task UpdateAutorun()
        {
            await Delay(250);
            if (_autoRun)
            {
                _pos = Game.Player.Character.Position;
                if (!_autoRunning || Utility.Vector3Distance2(_pos, _autoRunDestination) < 100.0f)
                {
                    _autoRunDestination = _pos;
                    _autoRunning = true;
                    Utility.Vector3Project(ref _autoRunDestination, Game.Player.Character.Heading, 30.0f);
                    Game.Player.Character.Task.RunTo(_autoRunDestination, true);
                }
            }
        }

        private async Task OnFrame()
        {
            if (_buttonDown)
            {
                i++;
                if (API.IsControlJustReleased(0, button))
                {
                    _buttonDown = false;
                    if(i<30 && !_hasControl)
                        TakeControl();
                }
            }
            else if (API.IsControlJustPressed(0, button))
            {
                _buttonDown = true;
                i = 0;
            }


            await Task.FromResult(0);

        }

        private void OnBuy(string json)
        {
            string error = null;
            try
            {
                Cart cart = Cart.FromJson(json);
                error = cart.GiveItems();
            }
            catch(Exception e)
            {
                error = "Something went wrong with the cart, sorry!";
                Debug.Write(e.Message);
            }
            if (error == null)
            {
                ReleaseControl();
            }
            API.SendNuiMessage(JsonConvert.SerializeObject(new GeneralResult
            {
                obtype = "buyresult",
                err = error
            }));
        }

        private void OnReady(string body)
        {
            API.SetNuiFocus(false, false);
            try
            {
                string json = Inventory.ToJson();
                Debug.WriteLine(json);
                PlayerController.AddToBalance(0);
                API.SendNuiMessage(json);
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
            }
        }

        public void SendError(string error)
        {
            error = error.Replace('"', '`');
            API.SendNuiMessage(String.Format("{{\"obtype\":\"error\",\"msg\":\"{0}\"}}", error));
        }

        public static void SendInfo(string message)
        {
            message = message.Replace('"', '`');
            API.SendNuiMessage(String.Format("{{\"obtype\":\"info\",\"msg\":\"{0}\"}}", message));
        } 

        private void TakeControl()
        {
            _autoRun = API.IsControlPressed(0, 32);
            API.SendNuiMessage(Equipment.ToJson());
            API.SendNuiMessage("{\"obtype\":\"toggle\",\"show\":true}");
            API.SetCursorLocation(20, 20);
            API.SetNuiFocus(true, true);
            _hasControl = true;
        }

        internal void ReleaseControl()
        {
            if (!_hasControl) return;
            API.SendNuiMessage("{\"obtype\":\"toggle\",\"show\":false}");
            API.SetNuiFocus(false, false);
            _hasControl = false;
            if (_autoRunning)
                Game.Player.Character.Task.ClearAll();
            if (_autoRun)
            {
                API.EnableControlAction(0, 21, true);
                API.EnableControlAction(0, 32, true);
            }
            _autoRun = false;
            _autoRunning = false;
        }


    }
}
