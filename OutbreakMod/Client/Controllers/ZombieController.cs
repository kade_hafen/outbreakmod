﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client
{
    internal class Zombie
    {
        internal Ped zombie = null;
        internal bool rewardsHandled = false;
        internal bool mine = false;

        public Zombie(Ped ped, bool isMine)
        {
            zombie = ped;
            mine = isMine;
        }

        public Zombie(Ped ped)
        {
            zombie = ped;
        }

        public void Update()
        {
            if (zombie.IsDead)
            {
                if (!rewardsHandled)
                {

                    Entity killer = zombie.GetKiller();

                    if (killer != null && killer.Equals(Game.Player.Character))
                    {
                        PlayerController.AddToBalance(20);
                        rewardsHandled = true;
                    }

                    //mark for despawn
                    zombie.MarkAsNoLongerNeeded();
                }
                return;
            }

            float distance;
            Player closestPlayer = Utility.GetClosestPlayer(zombie.Position, out distance);

            if (closestPlayer == null || distance > 3000.0f)
            {
                zombie.MarkAsNoLongerNeeded();
                return;
            }
            
            if (distance <= 50 && !zombie.GetMeleeTarget().Equals(closestPlayer.Character))
            {
                zombie.Task.FightAgainst(closestPlayer.Character);
            }
            else if (distance < 2000)
            {
                API.SetPedMovementClipset(zombie.Handle, "move_m@alien", 1f);
                Vector3 pos = closestPlayer.Character.Position;
                API.TaskGoToCoordAnyMeans(zombie.Handle, pos.X, pos.Y, pos.Z, 20, 0, true, 786602, 0xbf800000);
                API.SetPedMoveRateOverride(zombie.Handle, 2.5f);
                zombie.BlockPermanentEvents = true;
                zombie.AlwaysKeepTask = true;
            }
        }
    }

    public class ZombieController : BaseScript
    {
        internal static Dictionary<int, Zombie> Zombies { get; } = new Dictionary<int, Zombie>();

        //keeps track of the number of zombies this player specifically has spawned.
        public static int NumZombies = 0;
        public static int NumMyZombies = 0;
        private int _count = 0;
        private int _myCount = 0;

        public ZombieController()
        {
            Tick += OnTick;
        }

        public static bool IsZombie(Ped ped)
        {
            return (uint)ped.Model.Hash == (uint)PedHash.Zombie01;
        }

        private async Task OnTick()
        {
            await Delay(250);

            _count = 0;
            _myCount = 0;
            Zombie zombie;
            foreach (Ped ped in World.GetAllPeds())
            {
                if (IsZombie(ped))
                {
                    if (Zombies.TryGetValue(ped.Handle, out zombie))
                    {
                        zombie.Update();
                        if (zombie.zombie.IsAlive) {
                            if(zombie.mine)
                                _myCount++;
                            else
                                _count++;
                        }
                    }
                    else
                    {
                        Zombies.Add(ped.Handle, new Zombie(ped));
                    }
                }
            }
            NumZombies = _count;
            NumMyZombies = _myCount;
        }
    }
}
