﻿using CitizenFX.Core;
using Common;
using System;
using System.Threading.Tasks;

namespace Client
{
    class ZombieSpawner : BaseScript
    {
        //public static int _MaxAllZombies = 320;
        public static int _MaxMyZombies = 40;

        public static int Density { get; private set; } = 1;

        public ZombieSpawner()
        {
            EventHandlers["outbreak:density"] += new Action<int>(OnDensity);

            Tick += OnTick;
            Tick += Spawn;
        }

        private async Task Spawn()
        {
            if (_MaxMyZombies > 0 && ZombieController.NumMyZombies < _MaxMyZombies)// && ZombieController.NumZombies < _MaxAllZombies)
            {
                Vector3 spawnLoc = Game.PlayerPed.Position;
                Utility.Vector3Project(ref spawnLoc, RNG.Float(0, 360), RNG.Float(50, 70));
                spawnLoc.Z = World.GetGroundHeight(spawnLoc);

                Ped ped = await ZombieFactory.Create(spawnLoc);

                Zombie zombie;
                if (ZombieController.Zombies.TryGetValue(ped.Handle, out zombie))
                    zombie.mine = true;
                else
                    ZombieController.Zombies.Add(ped.Handle, new Zombie(ped, true));
            }
            await Delay(250);
        }

        private void OnDensity(int density)
        {
            Density = density;

            switch (density)
            {
                case 1:
                    _MaxMyZombies = 1;
                    break;
                case 2:
                    _MaxMyZombies = 3;
                    break;
                case 3:
                    _MaxMyZombies = 4;
                    break;
                case 4:
                    _MaxMyZombies = 6;
                    break;
                case 5:
                    _MaxMyZombies = 10;
                    break;
                default:
                    _MaxMyZombies = 0;
                    break;
            }
        }
        
        private async Task OnTick()
        {
            Vector3 pos = Game.PlayerPed.Position;
            TriggerServerEvent("outbreak:position", new object[] { pos.X, pos.Y, pos.Z });
            await Delay(5000);
        }
    }
}
