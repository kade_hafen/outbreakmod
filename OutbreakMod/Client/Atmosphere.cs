﻿using Common;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System.Threading.Tasks;

namespace Client
{
    class Atmosphere : BaseScript
    {

        public Atmosphere()
        {
            OutbreakScale.Initialize();
            Tick += OnFrame;
        }
        
        private async Task OnFrame()
        {
            World.Blackout = !OutbreakScale.LightsOn;
            //set pedestrian density
            API.SetPedDensityMultiplierThisFrame(OutbreakScale.PedestrianDensity);
            API.SetScenarioPedDensityMultiplierThisFrame(OutbreakScale.PedestrianDensity, OutbreakScale.PedestrianDensity);
            //set traffic density
            API.SetVehicleDensityMultiplierThisFrame(OutbreakScale.TrafficDensity);
            API.SetRandomVehicleDensityMultiplierThisFrame(OutbreakScale.TrafficDensity);
            //set parked car density
            API.SetParkedVehicleDensityMultiplierThisFrame(OutbreakScale.ParkedVehicleDensity);

            await Task.FromResult(0);
        }
    }
}
