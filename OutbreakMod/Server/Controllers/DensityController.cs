﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;

namespace Server
{
    public class DensityController : BaseScript
    {
        public DensityController()
        {
            EventHandlers["outbreak:position"] += new Action<Player, float, float, float>(OnPosition);
        }

        private static void OnPosition([FromSource]Player player, float X, float Y, float Z)
        {
            PlayerData data = PlayersController.GetData(player);
            if (data != null)
            {
                data.Position.X = X;
                data.Position.Y = Y;
                data.Position.Z = Z;
                TriggerClientEvent(player, "outbreak:density", new object[] { 2 });
            }
        }
    }
}
