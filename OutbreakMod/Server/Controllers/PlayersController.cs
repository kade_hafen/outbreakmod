﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    internal class PlayerData
    {
        internal Vector3 Position = new Vector3();
        internal int Balance = 0;
    }

    internal class PlayersController : BaseScript
    {
        private static Dictionary<Player, PlayerData> players = new Dictionary<Player, PlayerData>();

        public PlayersController()
        {
            EventHandlers["outbreak:balance:add"] += new Action<Player, int>(OnBalanceAdd);
        }

        private static void OnBalanceAdd([FromSource]Player player, int amount)
        {
            PlayerData data = GetData(player);
            if (data != null)
            {
                data.Balance += amount;
                TriggerClientEvent(player, "outbreak:balance", new object[] { data.Balance });
            }
        }

        internal static PlayerData GetData(Player player)
        {
            PlayerData data;
            if (!players.ContainsKey(player))
            {
                data = new PlayerData();
                players.Add(player, new PlayerData());
            }
            else
            {
                if (!players.TryGetValue(player, out data))
                    data = null;
            }
            return data;
        }

    }
}
