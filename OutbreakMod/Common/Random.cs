﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class RNG
    {
        private static Random random = new Random();
        public static float Float(float min, float max)
        {
            return (float)(random.NextDouble() * (max - min) + min);
        }

        public static float Float()
        {
            return Float(0.0f, 1.0f);
        }

        public static int Int(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static int Int()
        {
            return Int(0, 1);
        }
    }
}
