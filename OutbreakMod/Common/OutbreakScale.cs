﻿using System;

namespace Common
{
    public static class OutbreakScale
    {
        static float _Scale = 1.0f;

        public static void Initialize()
        {
            Calculate();
        }

        public static void ChangeScale(float newScale)
        {
            _Scale = newScale;
            Calculate();
        }

        private static void Calculate()
        {
            PedestrianDensity = Math.Max(0, (0.8f - _Scale) + 0.2f);
            TrafficDensity = Math.Max(0, (0.6f - _Scale) + 0.4f);
            ParkedVehicleDensity = Math.Max(0, (0.1f - _Scale) + 0.9f);
            LightsOn = _Scale <= RNG.Float(0.2f, 0.3f);
        }

        public static float PedestrianDensity { get; private set; } = 0.0f;
        public static float TrafficDensity { get; private set; } = 0.0f;
        public static float ParkedVehicleDensity { get; private set; } = 0.0f;
        public static bool LightsOn { get; private set; } = false;
    }
}
