resource_manifest_version "05cfa83c-a124-4cfa-a768-c24a5811d8f9"

client_scripts {
	"OutbreakCommon.net.dll",
	"OutbreakClient.net.dll"
}

ui_page("ui/index.html")

files({
	"ui/index.html",
	"Newtonsoft.Json.dll"
})

server_scripts {
	"OutbreakCommon.net.dll",
	"OutbreakServer.net.dll"
}